# Basic language extension for JastAdd

Syntax highlighting support for https://jastadd.cs.lth.se/

## Features

* Defines a new language `jrag`.
* Adds highlighting for `jrag` & `jadd` files. This reuses builtin Java highlighting, so only the jrag-specific keywords are actually contributed by this extension.
* Defines a new language `ast`.
* Add highlighting for `ast` files.

## Building

### Prerequisites
- `npm` installed

### Steps
- Run `npm run build` in this directory. This creates a file `vsix/jastadd-language-support-N.N.N.vsix`, where `N-N-N` matches the version in package.json.

## Installing a vsix file

- Open VS Code
- Open the extension tab
- In the top right of the extension tab there is a three-dot "..." menu. Click it, and choose "Install from VSIX..".
- Reload the window when prompted, and then you should see keywords highlighted in jrag/jadd/ast files.
